import { createStore } from 'vuex';


export default createStore({
  state: {
    //isConnected: false,
    dateDebut:null,
    dateLimit:null,
    etiquetteColor:null,
    etiquetteLabel:null,
    currentUser:null,
    users:[],
    lastListId:4,
    lastCardId:6,
    CardMembers:[],
    EspaceDesTravaux:[],
    CurrentEspace:null,
    CurrentCard:{},
    CurrentList:null,
    cardDescription:null,
    GetCurrentSpaceName:null,
    Boards:[],
    EspaceTable:[],
    Lists:[],
    Cards:[],
    activity:[],
    CurrentCardMembers: [],
    Commentes:[],

  },methods: {
    
  },
  getters: {
    Lists(state){
      return state.Lists;
    },
    Cards (state){
      return state.Cards;
    },
  },
  mutations: {
    // SET_CURRENT_USER(state, payload) {
    //   state.current_user = payload
    // }
    GET_EspaceTable(state,status){
      state.EspaceTable = status;
    },
    LIST_USERS(state , status){
      state.users = status;
    },
    SET_CURRENT_USER(state, payload) {
      state.currentUser = payload
    },
    GET_CURENTUSER_BOARD(state , status){
      state.Boards = status;
    },
    SET_CURENTUSER_LIST(state , status){
      state.Lists = status;
    },
    //CurrentList
    SET_CURENT_LIST(state , status){
      state.CurrentList = status;
    },
    GET_CURENTUSER_CARDS(state , status){
      state.Cards = status;
    },
    GET_CURENTUSER_ESPACEDESTRAVAUX(state , status){
      state.EspaceDesTravaux = status;
    },
    GET_CURRENTESPACE(state,status){
      state.CurrentEspace = status;
    },
    GET_CURRENTSPACETITLE(state ,status){
      state.GetCurrentSpaceName = status ; 
    },
    GET_CURRENTCARD(state, status){
      state.CurrentCard = status;
    },
    SET_CARD_DESCRIPTION(state, status){
      state.cardDescription =  status ;
    },
    GET_CURRENT_CARD_ACTIVITY(state , status){
      state.activity = status;
    },
    SET_CURRENTCARD_MEMMBERS(state , status){
      state.CardMembers = status;
    },
    SET_CurrentCardMembers(state,newArray)
    {
        state.CurrentCardMembers == [...state.CurrentCardMembers, ...newArray];
    },
    ADD_MEMBER_TO_CARD(state,status){
      state.CurrentCardMembers.push(status);
    },
    REMOVE_MEMBER_TO_CARD(state,index){
      state.CurrentCardMembers.splice(index,1);
    },
    GET_COMMENTES(state, status) {
      state.Commentes = status;
    }

  },
  actions: {
  },
  modules: {
  },mounted() {
    
  },
})
