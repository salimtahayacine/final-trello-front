import { createRouter, createWebHistory } from 'vue-router'
import store from '@/store'
import HomeView from '../views/HomeView.vue'
import Boards from '@/views/Boards.vue'
import Register from '@/views/Register.vue'
import Board from '@/components/Board.vue'
import NewBoards from "@/views/CreatNewBoard.vue"
import LoginView from "@/views/LoginView.vue"
import EspaceDesTravaux from '@/views/EspaceDesTravaux.vue'
import UserProfile from '@/views/UserProfile'

const routes = [
  {
    path: '/',
    name: 'EspaceDesTravaux',
    component: EspaceDesTravaux,
    beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/Boards/:id',
    name: 'Boards',
    component: Boards,
    beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
  },
  {
    path: '/newBoard',
    name: 'NewBoards',
    component: NewBoards,
    beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
  },
  {
    path: '/Board/:id',
    name: 'Board',
    component: Board,
    beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
  },
  {
    path: '/userprofile',
    name: 'UserProfile',
    component: UserProfile,
    beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
  },
]

//--GUARDS--- BEFOREENTER
/*
beforeEnter: (to, from, next) => { 
      if (store.state.currentUser == null) {
        next('/login');
      }
      next();
    }
*/

const GetSessionUser = () => {
  //Check user Session GETSESSION
  console.log("get session: ", )
  fetch("http://127.0.0.1:9090/getSessionUser",{
    method: 'POST',
   credentials: 'include',
   headers: {
     'Content-Type': 'application/json'
   },
   body: JSON.stringify({ })
  }).then(response => {
   return response.json();
  }).then(data => {
    if(data.succeeded == true){
     //store.state.currentUser = data.data
     store.commit('SET_CURRENT_USER',data.data);
     console.log("CurentUser from-App-mounted:" ,data.data)
     //console.log("CurentUser from-App-mounted:" ,data.data._id)
    }else if(data.succeeded == false){
     //store.state.currentUser = null
     swal("Oups","You must be logged in ");
     this.$router.push('/login')
     
    }
   }).catch(error => {
     this.$router.push('/login')
     swal("error","You must be loggedin Error : "+error);
   });
  }
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});
router.beforeEach( () => {
    GetSessionUser();
    console.log("CurrentUSer",store.state.currentUser);
});


export default router
